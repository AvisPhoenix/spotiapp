import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {

  constructor( private _spotifySrv:SpotifyService ) { }

  ngOnInit() {
    this._spotifySrv.getArtistas('Metallica').subscribe();
  }

}
