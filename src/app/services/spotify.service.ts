import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {

  artistas:any[] = [];

  urlBusqueda:string="https://api.spotify.com/v1/search";
  urlArtista:string="https://api.spotify.com/v1/artist";
  token:string = "BQDmijPi23Y0roT7SmCrCEW6nOVU5MOco_iN4ocDrioa8ozPv9MajzZ1MBRfnmk-MAZKJpHMvIIJxrM_xIDGNQ";

  constructor(private http:Http) { }

  getArtistas( termino:string ){

    let headers = new Headers();
    //headers.append('Content-Type','application/x-www-form-urlencoded');
    headers.append('autorization','Bearer BQDmijPi23Y0roT7SmCrCEW6nOVU5MOco_iN4ocDrioa8ozPv9MajzZ1MBRfnmk-MAZKJpHMvIIJxrM_xIDGNQ');

    let query = `q=${ termino }&type=artist`
    let url = this.urlBusqueda + "?" + query;

    return this.http.get( url, { headers } ).map( res => {
      console.log(res);
      this.artistas = res.json().artists.items;
    } );
  }

}
